<%@ Application Language="VB" %>
<%@ Import Namespace="CompassCC.CCCSystem.CCCCommon" %>
<%@ Import Namespace="CompassCC.ProSolution.PSWebEnrolmentKit" %>

<script runat="server">

    Dim m_blnInitialised As Boolean

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup

        Initialise()

        m_blnInitialised = True

    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
        Try
            'Get the last error that occured and save it in a session
            Dim exceptionContainer As Exception = Server.GetLastError

            'Write the error to the session if it is available
            If Not System.Web.HttpContext.Current.Session Is Nothing Then
                Session("LastError") = exceptionContainer

                'Need to do this to not destroy the session
                Server.ClearError()

                Response.Redirect("~/GenericError.aspx")
            End If

        Catch ex As Exception
            Diagnostics.Debug.WriteLine(ex.Message)
        End Try
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started

        If Not m_blnInitialised Then
            'In case of errors during startup (E.g. Database server not running)
            Initialise()
            m_blnInitialised = True
        End If

        Session.LCID = 2057 'Set Locale to UK (Ignoring regional settings of computer)

    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub

    Private Sub Initialise()



        CCCDataCache.DisableAllCaching = True
        CCCConfigLibrary.ReadConfigFromConfigFile(IO.Path.Combine(HttpRuntime.AppDomainAppPath, "web.config"))
        CCCConfigLibrary.ReadConfigFromConfigFile(IO.Path.Combine(HttpRuntime.AppDomainAppPath, "web.dbconfig"))
        CCCConfigLibrary.ReadConfigFromMyAppDBConfigFile()
        CCCDatabaseConnectInfoList.LoadDatabaseInfoFromConfig()


        'Set timeout values from config
        CCCConfigLibrary.SetAllFrameworkValuesFromConfig()

        CCCObjectFactory.InitialiseForAssembly(GetType(CompassCC.ProSolution.PSWebEnrolmentKit.EnrolmentRequestDataTable).Assembly, AssemblyType.Business)

        'Configure/check connections
        Dim rules As CCCBrokenRulesList = CCCDatabaseConnectionLibrary.CheckDBConnectionInfos(CCCDatabaseConnectInfoList.GetDatabaseConnectInfos(), False)
        If Not rules.IsAllValid Then
            Throw New CCCTechnicalException("One or more database connections is not valid: " & rules.GetBrokenRulesText())
        End If

        If CCCDatabaseConnectInfo.DefaultDatabaseDBInfo IsNot Nothing Then
            'Load metadata from default database
            CCCMetaData.InitialiseForDatabase(CCCDatabaseConnectInfo.DefaultDatabaseDBInfo)
        End If

        CCCEnvironmentLibrary.ApplicationInfo_ProductName = "ProSolution Web Toolkit"
        'Custom Control Settings

        CCCWebField.ShouldCreateRequiredFieldIcon = False
        CCCWebField.UseProSolutionMetadata = true

        CheckoutBaseControl.ShouldCheckBasketNotEmpty = False

        WorkingData.Provider = New SessionWorkingDataProvider

        PaymentSubmitter.ShouldSendEmailConfirmation = False
        PaymentSubmitter.AllowEmptyBasket = True
        PaymentSubmitter.ShouldValidateAge = False
        PaymentSubmitter.MinimumAge = 16
        PaymentSubmitter.ShouldMakeNewApplicationForEachOffering = True
        PaymentSubmitter.AllowEmptyBasket = True
        Toastr.UseToastr = True


        'System Settings
        SystemSettings.AcademicYearID = "16/17"

        SystemSettings.CollegeOrganisationID = 0

        'DB Timeouts
        SystemSettings.DefaultTimeout = 60
        SystemSettings.DefaultLongTimeout = 1200

        'Proxy Server for web requests to pass through
        SystemSettings.ProxyDomain = ""
        SystemSettings.ProxyPassword = ""
        SystemSettings.ProxyURL = ""
        SystemSettings.ProxyUserName = ""

        'QAS Pro Web Settings
        SystemSettings.QAS_Layout = "Database Layout"
        SystemSettings.QAS_ServerURL = ""

        'QAS On Demand Settings
        SystemSettings.QASOnDemand_Password = ""
        SystemSettings.QASOnDemand_URL = ""
        SystemSettings.QASOnDemand_Username = ""

        'AFD Postcode Evolution Settings
        SystemSettings.AFDEvolution_Password = ""
        SystemSettings.AFDEvolution_SerialNumber = ""
        SystemSettings.AFDEvolution_Server = ""
        SystemSettings.AFDEvolution_UserID = ""

        'Capscan On Demand Settings
        SystemSettings.Capscan_AccessCode = ""
        SystemSettings.Capscan_ClientID = ""

        'Generic Payment Settings
        SystemSettings.Payment_InstitutionID = "TEST-GBP"
        SystemSettings.Payment_Secret = "TEST-GBP-TEST-KEY"
        SystemSettings.Payment_SubmitURL = "https://test.bnsgateway.co.uk/PaymentGateway/Default.aspx"

        'WorldPay-specific Settings
        SystemSettings.WorldPay_TestMode = "100"

        'RealEx-specific Settings
        SystemSettings.RealEx_Account = ""

        'Verify Email for new acccounts
        SystemSettings.ShouldVerifyEmail = True

        'Address provider to use (one only)
        SystemSettings.UseAFD = False
        SystemSettings.UseAFDEvolution = False
        SystemSettings.UseCapscan = False
        SystemSettings.UseQASProOnDemand = False

        'Payment Provider to use (one only)
        SystemSettings.UseCapita = False
        SystemSettings.UseCivica = False
        SystemSettings.UsePayPal = False
        SystemSettings.UseRealEx = False
        SystemSettings.UseWorldPay = False
        SystemSettings.UseBucksNet = True

        'Civica-specific settings
        SystemSettings.Civica_Account = ""
        SystemSettings.Civica_FundCode = ""
        SystemSettings.Civica_AccountReference = ""

        'Capita-specific settings
        SystemSettings.CapitaPaymentsV2 = True

    End Sub


</script>