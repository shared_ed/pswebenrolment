<%@ control Language="VB" AutoEventWireup="false" CodeFile="checkout_applications.ascx.vb" Inherits="checkout_applications"  %>
<%@ Register Assembly="PSWebEnrolmentKit" Namespace="CompassCC.ProSolution.PSWebEnrolmentKit"
    TagPrefix="cc1" %>


<br /><br />
                <ol class="breadcrumb">
                   <li><a href="http://www.liv-coll.ac.uk/All_Courses.aspx">Search</a></li>
                    <li><a href="#" onclick="window.history.go(-2);return false;">Course Details</a></li>
                    <li><a href="#" onclick="window.history.go(-1);return false;">Your Courses</a></li>
                  <li class="active">Personal Details</li>
                </ol>

  
<div class="panel panel-success">
    <div class="panel-heading">Personal Details</div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="surname" ID="fldSurname" runat="server" IsRequired="true" LabelWidth="200"  AutoFocus="true"/>
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="FirstForename" ID="StudentEnrolmentField1" runat="server" IsRequired="true" LabelWidth="200"  CustomCaption='First Name' />
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="Title" ID="StudentEnrolmentField7" runat="server" IsRequired="true" LabelWidth="200" />
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField id="datepicker"  runat="server" IsRequired="true" StudentEnrolmentFieldType="DateOfBirth"  LabelWidth="200" ClientIDMode="Static"  Placeholder="dd/mm/yyyy"/>       
    </div>
   
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="Sex" ID="StudentEnrolmentField4" runat="server" LabelWidth="200" />
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="Tel" ID="StudentEnrolmentField5" runat="server"  LabelWidth="200" Pattern="^((\(?0\d{4}\)?\s?\d{3}\s?\d{3})|(\(?0\d{3}\)?\s?\d{3}\s?\d{4})|(\(?0\d{2}\)?\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$"/>
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField StudentEnrolmentFieldType="MobileTel" ID="StudentEnrolmentField2" runat="server"  LabelWidth="200" Pattern="^(07[\d]{8,12}|447[\d]{7,11})$"/>
    </div>
    <div class=" form-group">
        <cc1:StudentEmailField StudentEnrolmentFieldType="Email" ID="StudentEnrolmentField3" runat="server" IsRequired="true" LabelWidth="200" HTML5InputType="email" Placeholder="e.g. name@domain.com" Pattern="^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$" />
    </div>
    <div class=" form-group">
         <cc1:StudentEnrolmentField runat="server" ID="fldAddress1" StudentEnrolmentFieldType="Address1" IsRequired="true"/>
     </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField runat="server" ID="fldAddress2" StudentEnrolmentFieldType="Address2" />
    </div>  
    <div class=" form-group">
        <cc1:StudentEnrolmentField runat="server" ID="fldAddress3" StudentEnrolmentFieldType="Address3" />
    </div>
    <div class=" form-group">
        <cc1:StudentEnrolmentField runat="server" ID="fldAddress4" StudentEnrolmentFieldType="Address4" />
    </div>
    <div class=" form-group">
        <label for="postcode" style="font-weight:normal ">Postcode</label>
        <input runat="server" type="text" id="postcode" class="form-control" name="pre[postalcode]" placeholder="Your postcode here..." autocomplete="off" IsRequired="true"/>
    </div>
     <div class=" form-group">
        <label for="fldStudentDeclaration" style="font-weight:normal;position:relative;top:20px; ">Please tell us why you have chosen this course.</label>
        <cc1:StudentEnrolmentField runat="server" ID="fldStudentDeclaration" StudentEnrolmentFieldType="StudentDeclaration"  CustomCaption="&nbsp;" AutoFocus="False" CustomFieldType="NotSpecified" HTML5InputType="text"  IsHidden="False" IsReadOnly="False" IsRequired="False" />
    </div>
    <div class=" form-group" style="padding-top:20px;">
        <label for="fldCriminalConvictionID" style="font-weight:normal "><b>Do you have a criminal conviction, or a pending court case, related to a violent or sexual offence, or one involving drug dealing?</b></label>
        <cc1:StudentEnrolmentField runat="server" ID="fldCriminalConvictionID" StudentEnrolmentFieldType="CriminalConvictionID" IsRequired="true" CustomCaption="Criminal Conviction"/>
    </div>
    
</div>

    <!--
<div class="panel panel-success">

    <div class="panel-heading">Photo</div>
    <p>If you would like to upload a photo, you can do that here. </p>
    <div class=" form-group">
    <asp:FileUpload ID="FileUpload1" runat="server" />
        </div>
    </div>
   -->


        <cc1:CCCButton id="btnBack" runat="server" Text="Back" ImageResource="btnBack" LinkResource="checkout_aspx"/>
        <cc1:CCCButton ID="btnContinue" runat="server" Text="Continue" ImageResource="btnContinue" LinkResource="checkout_applications2" CausesValidation="true" />
    <br />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="" />

