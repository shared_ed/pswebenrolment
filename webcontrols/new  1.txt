#region Help:  Introduction to the Script Component
/* The Script Component allows you to perform virtually any operation that can be accomplished in
 * a .Net application within the context of an Integration Services data flow.
 *
 * Expand the other regions which have "Help" prefixes for examples of specific ways to use
 * Integration Services features within this script component. */
#endregion

#region Namespaces
using System;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Net.Http;
using Microsoft.SqlServer.Dts.Runtime;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using SmartAssessorShared;
#endregion

/// <summary>
/// This is the class to which to add your code.  Do not change the name, attributes, or parent
/// of this class.
/// </summary>
[Microsoft.SqlServer.Dts.Pipeline.SSISScriptComponentEntryPointAttribute]
public class ScriptMain : UserComponent
{
    #region Help:  Using Integration Services variables and parameters
    /* To use a variable in this script, first ensure that the variable has been added to
     * either the list contained in the ReadOnlyVariables property or the list contained in
     * the ReadWriteVariables property of this script component, according to whether or not your
     * code needs to write into the variable.  To do so, save this script, close this instance of
     * Visual Studio, and update the ReadOnlyVariables and ReadWriteVariables properties in the
     * Script Transformation Editor window.
     * To use a parameter in this script, follow the same steps. Parameters are always read-only.
     *
     * Example of reading from a variable or parameter:
     *  DateTime startTime = Variables.MyStartTime;
     *
     * Example of writing to a variable:
     *  Variables.myStringVariable = "new value";
     */
    #endregion

    #region Help:  Using Integration Services Connnection Managers
    /* Some types of connection managers can be used in this script component.  See the help topic
     * "Working with Connection Managers Programatically" for details.
     *
     * To use a connection manager in this script, first ensure that the connection manager has
     * been added to either the list of connection managers on the Connection Managers page of the
     * script component editor.  To add the connection manager, save this script, close this instance of
     * Visual Studio, and add the Connection Manager to the list.
     *
     * If the component needs to hold a connection open while processing rows, override the
     * AcquireConnections and ReleaseConnections methods.
     * 
     * Example of using an ADO.Net connection manager to acquire a SqlConnection:
     *  object rawConnection = Connections.SalesDB.AcquireConnection(transaction);
     *  SqlConnection salesDBConn = (SqlConnection)rawConnection;
     *
     * Example of using a File connection manager to acquire a file path:
     *  object rawConnection = Connections.Prices_zip.AcquireConnection(transaction);
     *  string filePath = (string)rawConnection;
     *
     * Example of releasing a connection manager:
     *  Connections.SalesDB.ReleaseConnection(rawConnection);
     */
    #endregion

    #region Help:  Firing Integration Services Events
    /* This script component can fire events.
     *
     * Example of firing an error event:
     *  ComponentMetaData.FireError(10, "Process Values", "Bad value", "", 0, out cancel);
     *
     * Example of firing an information event:
     *  ComponentMetaData.FireInformation(10, "Process Values", "Processing has started", "", 0, fireAgain);
     *
     * Example of firing a warning event:
     *  ComponentMetaData.FireWarning(10, "Process Values", "No rows were received", "", 0);
     */
    #endregion



    public override void CreateNewOutputRows()
    {
        try
        {
            JToken learners = JToken.Parse(SmartAssessor.GetRequest("Learner/GetLearners"));

            foreach (JToken learner in learners.Children())
            {
                String learnerId = (string)learner.SelectToken("Id");
                String strJson = "{'Id': '" + learnerId + "'}";
                JToken sessions = JToken.Parse(SmartAssessor.PostRequest("Learner/GetSessions", strJson));

                foreach (JToken session in sessions.Children())
                {
                    Output0Buffer.AddRow();
                    Output0Buffer.LearnerId = learnerId;
                    Output0Buffer.Id = (string)session.SelectToken("Id");
                    Output0Buffer.ParentModule = (string)session.SelectToken("ParentModule");
                    Output0Buffer.ParentLevel = (string)session.SelectToken("ParentLevel");
                    Output0Buffer.ParentTrainerId = (string)session.SelectToken("ParentTrainerId");
                    Output0Buffer.StartTime = (string)session.SelectToken("StartTime");
                    Output0Buffer.Duration = (string)session.SelectToken("Duration");
                    Output0Buffer.Places = (string)session.SelectToken("Places");
                    Output0Buffer.Subcategory = (string)session.SelectToken("Subcategory");
                    Output0Buffer.RoomNumber = (string)session.SelectToken("RoomNumber");
                    Output0Buffer.NumberOfVirtualParticipants = (string)session.SelectToken("NumberOfVirtualParticipants");
                    Output0Buffer.EndTime = (string)session.SelectToken("EndTime");
                    Output0Buffer.DateCreated = (string)session.SelectToken("DateCreated");
                    Output0Buffer.CreatedBy = (string)session.SelectToken("CreatedBy");
                    Output0Buffer.DateLastUpdated = (string)session.SelectToken("DateLastUpdated");
                    Output0Buffer.LastUpdatedBy = (string)session.SelectToken("LastUpdatedBy");

                    string buffer = (string)session.SelectToken("FormativeNotes");
                    if (buffer != null)
                    {
                        Output0Buffer.FormativeNotes = buffer.Length < 1000 ? buffer : buffer.Substring(0, 999);
                    }

                    buffer = (string)session.SelectToken("SummativeNotes");
                    if (buffer != null)
                    {
                        Output0Buffer.SummativeNotes = buffer.Length < 1000 ? buffer : buffer.Substring(0, 999);
                    }

                    buffer = (string)session.SelectToken("Location");
                    if (buffer != null)
                    {
                        Output0Buffer.Location = buffer.Length < 100 ? buffer : buffer.Substring(0, 99);
                    }

                    Output0Buffer.UnitId = (string)session.SelectToken("UnitId");
                    Output0Buffer.SunesisId = (string)session.SelectToken("SunesisId");
                    Output0Buffer.SessionCourseId = (string)session.SelectToken("SessionCourseId");
                    Output0Buffer.OfflineTSId = (string)session.SelectToken("OfflineTSId");
                    Output0Buffer.Provisional = (string)session.SelectToken("Provisional");
                    Output0Buffer.GroupId = (string)session.SelectToken("GroupId");
                    Output0Buffer.Parent = (string)session.SelectToken("Parent");
                    Output0Buffer.Children = (string)session.SelectToken("Children");
                }
            }
        }
        catch (Exception e)
        {
            FailComponent(e.ToString());
        }

    }

    private void FailComponent(string errorMsg)
    {
        bool fail = false;
        IDTSComponentMetaData100 compMetadata = this.ComponentMetaData;
        compMetadata.FireError(1, "Error Getting Data From Webservice!", errorMsg, "", 0, out fail);

    }

}
