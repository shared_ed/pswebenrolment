﻿Option Explicit On
Option Strict On

Imports CompassCC.CCCSystem.CCCCommon
Imports CompassCC.ProSolution.PSWebEnrolmentKit

Partial Class webcontrols_checkout_enrolments
    Inherits CheckoutBaseControl

    Protected Overrides Sub OnLoad(e As EventArgs)

        If PaymentSubmitter.AllowEmptyBasket And WorkingData.ShoppingCart.Items.Count = 0 Then
            Session("RequestMode") = RequestMode.EnrolmentRequest
        End If


        If Not IsPostBack Then
            postcode.Value = WorkingData.EnrolmentRequestRow.PostcodeOut & WorkingData.EnrolmentRequestRow.PostcodeIn        
        End If
        MyBase.OnLoad(e)
    End Sub

    Private Sub btnContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinue.Click

        Me.Page.Validate()

        If Me.Page.IsValid Then


            'postcode stuff            
            If Len(postcode.Value) > 0 Then
                WorkingData.EnrolmentRequestRow.PostcodeOut = postcode.Value.Substring(0, postcode.Value.Length - 3)
                WorkingData.EnrolmentRequestRow.PostcodeIn = Right(postcode.Value, 3)
            End If

        End If

    End Sub

End Class
